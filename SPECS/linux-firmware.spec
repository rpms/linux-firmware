%global checkout 5bc5868b

%global firmware_release 128

%global _firmwarepath	/usr/lib/firmware
%define _binaries_in_noarch_packages_terminate_build 0

Name:		linux-firmware
Version:	20250217
Release:	%{firmware_release}.git%{checkout}%{?dist}
Summary:	Firmware files used by the Linux kernel
License:	GPL+ and GPLv2+ and MIT and Redistributable, no modification permitted
URL:		http://www.kernel.org/
BuildArch:	noarch

# Tarball created from upstream git checkout using the following steps:
# 1) echo "/liquidio/lio_23xx_vsw.bin export-ignore" > .gitattributes # (GPL violation, see bug 1637694)
#    This is still causing problems in RHEL9 (see bug 1959913) and because of that we should keep out of RHEL8 too
# 2) git archive --worktree-attributes --format=tar --prefix=linux-firmware-%%{checkout}/ %%{checkout} | xz > linux-firmware-%%{version}.tar.xz
Source0:	%{name}-%{version}.tar.xz

Provides:	kernel-firmware = %{version} xorg-x11-drv-ati-firmware = 7.0
Obsoletes:	kernel-firmware < %{version} xorg-x11-drv-ati-firmware < 6.13.0-0.22
Obsoletes:	ueagle-atm4-firmware < 1.0-5
Obsoletes:	netxen-firmware < 4.0.534-9
Obsoletes:	ql2100-firmware < 1.19.38-8
Obsoletes:	ql2200-firmware < 2.02.08-8
Obsoletes:	ql23xx-firmware < 3.03.28-6
Obsoletes:	ql2400-firmware < 5.08.00-2
Obsoletes:	ql2500-firmware < 5.08.00-2
Obsoletes:	rt61pci-firmware < 1.2-11
Obsoletes:	rt73usb-firmware < 1.8-11
Obsoletes:	cx18-firmware < 20080628-10
Conflicts:	microcode_ctl < 2.1-0
# ivtv-firmware also provided the file v4l-cx25840.fw in older releases until
# version 2:20080701-28 in Fedora, when this conflicting file was removed in
# favour of the same file provided by linux-firmware. Fedora dropped the
# Obsoletes (see Fedora bugs 1211055, 1232773). RHEL 8 doesn't build so far any
# ivtv-firmware version, and we need to obsolete the conflicting versions to
# avoid upgrade errors (see bug 1589055)
Obsoletes:	ivtv-firmware < 2:20080701-28

BuildRequires: git make python3

%description
This package includes firmware files required for some devices to
operate.

%package -n iwl100-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 100 Series Adapters
License:	Redistributable, no modification permitted
Version:	39.31.5.1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl100-firmware < 39.31.5.1-4
%description -n iwl100-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl100 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl105-firmware
Summary:	Firmware for Intel(R) Centrino Wireless-N 105 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
%description -n iwl105-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl105 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl135-firmware
Summary:	Firmware for Intel(R) Centrino Wireless-N 135 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
%description -n iwl135-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl135 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl1000-firmware
Summary:	Firmware for Intel® PRO/Wireless 1000 B/G/N network adaptors
License:	Redistributable, no modification permitted
Version:	39.31.5.1
Epoch:		1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl1000-firmware < 1:39.31.5.1-3
%description -n iwl1000-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl1000 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl2000-firmware
Summary:	Firmware for Intel(R) Centrino Wireless-N 2000 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
%description -n iwl2000-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl2000 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl2030-firmware
Summary:	Firmware for Intel(R) Centrino Wireless-N 2030 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
%description -n iwl2030-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux to support the iwl2030 hardware.  Usage of the firmware
is subject to the terms and conditions contained inside the provided
LICENSE file. Please read it carefully.

%package -n iwl3160-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 3160 Series Adapters
License:	Redistributable, no modification permitted
Epoch:		1
Version:	25.30.13.0
Release:	%{firmware_release}%{?dist}.1
%description -n iwl3160-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl3945-firmware
Summary:	Firmware for Intel® PRO/Wireless 3945 A/B/G network adaptors
License:	Redistributable, no modification permitted
Version:	15.32.2.9
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl3945-firmware < 15.32.2.9-7
%description -n iwl3945-firmware
This package contains the firmware required by the iwl3945 driver
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl4965-firmware
Summary:	Firmware for Intel® PRO/Wireless 4965 A/G/N network adaptors
License:	Redistributable, no modification permitted
Version:	228.61.2.24
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl4965-firmware < 228.61.2.24-5
%description -n iwl4965-firmware
This package contains the firmware required by the iwl4965 driver
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl5000-firmware
Summary:	Firmware for Intel® PRO/Wireless 5000 A/G/N network adaptors
License:	Redistributable, no modification permitted
Version:	8.83.5.1_1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl5000-firmware < 8.83.5.1_1-3
%description -n iwl5000-firmware
This package contains the firmware required by the iwl5000 driver
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl5150-firmware
Summary:	Firmware for Intel® PRO/Wireless 5150 A/G/N network adaptors
License:	Redistributable, no modification permitted
Version:	8.24.2.2
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl5150-firmware < 8.24.2.2-4
%description -n iwl5150-firmware
This package contains the firmware required by the iwl5150 driver
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl6000-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 6000 AGN Adapter
License:	Redistributable, no modification permitted
Version:	9.221.4.1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl6000-firmware < 9.221.4.1-4
%description -n iwl6000-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl6000g2a-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 6005 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl6000g2a-firmware < 17.168.5.3-3
%description -n iwl6000g2a-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl6000g2b-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 6030 Series Adapters
License:	Redistributable, no modification permitted
Version:	18.168.6.1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl6000g2b-firmware < 17.168.5.2-3
%description -n iwl6000g2b-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl6050-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 6050 Series Adapters
License:	Redistributable, no modification permitted
Version:	41.28.5.1
Release:	%{firmware_release}%{?dist}.1
Obsoletes:	iwl6050-firmware < 41.28.5.1-5
%description -n iwl6050-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n iwl7260-firmware
Summary:	Firmware for Intel(R) Wireless WiFi Link 726x/8000/9000 Series Adapters
License:	Redistributable, no modification permitted
Epoch:		1
Version:	25.30.13.0
Release:	%{firmware_release}%{?dist}.1
# Obsolete iwl7265 sub-package which existed on RHEL 7, looking at git history
# Fedora never provided such sub-package (see bug 1589056)
Obsoletes:	iwl7265-firmware
%description -n iwl7260-firmware
This package contains the firmware required by the Intel wireless drivers
for Linux.  Usage of the firmware is subject to the terms and conditions
contained inside the provided LICENSE file. Please read it carefully.

%package -n libertas-usb8388-firmware
Summary:	Firmware for Marvell Libertas USB 8388 Network Adapter
License:	Redistributable, no modification permitted
Epoch:		2
Obsoletes:	libertas-usb8388-firmware < 2:5.110.22.p23-8
%description -n libertas-usb8388-firmware
Firmware for Marvell Libertas USB 8388 Network Adapter

%package -n libertas-usb8388-olpc-firmware
Summary:	OLPC firmware for Marvell Libertas USB 8388 Network Adapter
License:	Redistributable, no modification permitted
%description -n libertas-usb8388-olpc-firmware
Firmware for Marvell Libertas USB 8388 Network Adapter with OLPC mesh network
support.

%package -n libertas-sd8686-firmware
Summary:	Firmware for Marvell Libertas SD 8686 Network Adapter
License:	Redistributable, no modification permitted
Obsoletes:	libertas-sd8686-firmware < 9.70.20.p0-4
%description -n libertas-sd8686-firmware
Firmware for Marvell Libertas SD 8686 Network Adapter

%package -n libertas-sd8787-firmware
Summary:	Firmware for Marvell Libertas SD 8787 Network Adapter
License:	Redistributable, no modification permitted
%description -n libertas-sd8787-firmware
Firmware for Marvell Libertas SD 8787 Network Adapter

%prep
%autosetup -S git -p1 -n linux-firmware-%{checkout}

%build

%install
mkdir -p $RPM_BUILD_ROOT/%{_firmwarepath}
mkdir -p $RPM_BUILD_ROOT/%{_firmwarepath}/updates

%if 0%{?fedora} >= 34 || 0%{?rhel} >= 9
make DESTDIR=%{buildroot}/ FIRMWAREDIR=%{_firmwarepath} install-xz
%else
make DESTDIR=%{buildroot}/ FIRMWAREDIR=%{_firmwarepath} install
%endif

#Cleanup files we don't want to ship
pushd $RPM_BUILD_ROOT/%{_firmwarepath}
# Move amd-ucode readme to docs directory due to dracut issue (RHEL-15387)
mkdir -p %{buildroot}/%{_defaultdocdir}/%{name}/amd-ucode
%if 0%{?fedora} >= 34 || 0%{?rhel} >= 9
mv -f amd-ucode/README.xz %{buildroot}/%{_defaultdocdir}/%{name}/amd-ucode
%else
mv -f amd-ucode/README %{buildroot}/%{_defaultdocdir}/%{name}/amd-ucode
%endif
# Remove firmware shipped in separate packages already
# Perhaps these should be built as subpackages of linux-firmware?
rm -rf ess korg sb16 yamaha

# Remove source files we don't need to install
rm -f usbdux/*dux */*.asm
rm -rf carl9170fw

# No need to install old firmware versions where we also provide newer versions
# which are preferred and support the same (or more) hardware
rm -f libertas/sd8686_v8*
rm -f libertas/usb8388_v5.bin

# Remove firmware for Creative CA0132 HD as it's in alsa-firmware
rm -f ctefx.bin ctspeq.bin

# Remove cxgb3 (T3 adapter) firmware (see bug 1503721)
rm -rf cxgb3

# Remove obsolete and password-protected vgxe firmware (see bug 2108051)
rm -rf vxge

# Remove superfluous infra files
rm -f check_whence.py configure Makefile README

popd

# Create file list but exclude firmwares that we place in subpackages
# and netronome/nic_AMDA* symlinks
FILEDIR=`pwd`
pushd $RPM_BUILD_ROOT/%{_firmwarepath}
find . \! -type d > $FILEDIR/linux-firmware.files
find . -type d | sed -e '/^.$/d' > $FILEDIR/linux-firmware.dirs
popd
sed -i -e 's:^./::' linux-firmware.{files,dirs}
sed -i -e '/^iwlwifi/d' \
	-i -e '/^libertas\/sd8686/d' \
	-i -e '/^libertas\/usb8388/d' \
	-i -e '/^mrvl\/sd8787/d' \
	-i -e '/^netronome\/nic_AMDA/d' \
	linux-firmware.files
sed -i -e 's!^!/usr/lib/firmware/!' linux-firmware.{files,dirs}
sed -i -e 's/^/"/;s/$/"/' linux-firmware.files
sed -e 's/^/%%dir /' linux-firmware.dirs >> linux-firmware.files


%files -n iwl100-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-100-5.ucode

%files -n iwl105-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-105-*.ucode

%files -n iwl135-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-135-*.ucode

%files -n iwl1000-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-1000-*.ucode

%files -n iwl2000-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-2000-*.ucode

%files -n iwl2030-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-2030-*.ucode

%files -n iwl3160-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-3160-*.ucode
%{_firmwarepath}/iwlwifi-3168-*.ucode

%files -n iwl3945-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-3945-*.ucode

%files -n iwl4965-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-4965-*.ucode

%files -n iwl5000-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-5000-*.ucode

%files -n iwl5150-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-5150-*.ucode

%files -n iwl6000-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-6000-*.ucode

%files -n iwl6000g2a-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-6000g2a-*.ucode

%files -n iwl6000g2b-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-6000g2b-*.ucode

%files -n iwl6050-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-6050-*.ucode

%files -n iwl7260-firmware
%license WHENCE LICENCE.iwlwifi_firmware
%{_firmwarepath}/iwlwifi-7260-*.ucode
%{_firmwarepath}/iwlwifi-7265-*.ucode
%{_firmwarepath}/iwlwifi-7265D-*.ucode
%{_firmwarepath}/iwlwifi-8000C-*.ucode
%{_firmwarepath}/iwlwifi-8265-*.ucode
%{_firmwarepath}/iwlwifi-9000-*.ucode
%{_firmwarepath}/iwlwifi-9260-*.ucode
%{_firmwarepath}/iwlwifi-cc-a0-*.ucode
%{_firmwarepath}/iwlwifi-Qu*.ucode
%{_firmwarepath}/iwlwifi-ty-a0-gf-a0-*.ucode
%{_firmwarepath}/iwlwifi-ty-a0-gf-a0*.pnvm
%{_firmwarepath}/iwlwifi-so-a0-*.ucode
%{_firmwarepath}/iwlwifi-so-a0-*.pnvm
%{_firmwarepath}/iwlwifi-bz-b0-*.ucode*
%{_firmwarepath}/iwlwifi-bz-b0-*.pnvm*
%{_firmwarepath}/iwlwifi-gl-*.ucode
%{_firmwarepath}/iwlwifi-gl-*.pnvm
%{_firmwarepath}/iwlwifi-ma-*.ucode
%{_firmwarepath}/iwlwifi-ma-*.pnvm

%files -n libertas-usb8388-firmware
%license WHENCE LICENCE.Marvell
%dir %{_firmwarepath}/libertas
%{_firmwarepath}/libertas/usb8388_v9.bin

%files -n libertas-usb8388-olpc-firmware
%license WHENCE LICENCE.Marvell
%dir %{_firmwarepath}/libertas
%{_firmwarepath}/libertas/usb8388_olpc.bin

%files -n libertas-sd8686-firmware
%license WHENCE LICENCE.Marvell
%dir %{_firmwarepath}/libertas
%{_firmwarepath}/libertas/sd8686*

%files -n libertas-sd8787-firmware
%license WHENCE LICENCE.Marvell
%dir %{_firmwarepath}/mrvl
%{_firmwarepath}/mrvl/sd8787*

%files -f linux-firmware.files
%dir %{_firmwarepath}
%doc %{_defaultdocdir}/%{name}
%license WHENCE LICENCE.*
%config(noreplace) %{_firmwarepath}/netronome/nic_AMDA*

%changelog
* Mon Feb 17 2025 Denys Vlasenko <dvlasenk@redhat.com> - 20250217-128.git5bc5868b
- Update linux-firmware to latest upstream (RHEL-79835)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- i915: Update Xe2LPD DMC to v2.28
- ASoC: tas2781: Add regbin firmware by index for single device
- WHENCE: qca: add missing version information
- WHENCE: qca: add missing version information
- WHENCE: split generic QCA section into USB and serial sections
- rtl_bt: Update RTL8852B BT USB FW to 0x0474_842D
- iwlwifi: add Bz/gl FW for core93-123 release
- iwlwifi: update ty/So/Ma firmwares for core93-123 release
- iwlwifi: update cc/Qu/QuZ firmwares for core93-82 release
- ASoC: tas2781: Add dsp firmware for new projects
- amdgpu: DMCUB update for DCN401
- ath12k: WCN7850 hw2.0: update board-2.bin
- ath12k: QCN9274 hw2.0: update to WLAN.WBE.1.4.1-00199-QCAHKSWPL_SILICONZ-1
- ath12k: QCN9274 hw2.0: update board-2.bin
- ath11k: WCN6750 hw1.0: update board-2.bin
- ath11k: QCN9074 hw1.0: update to WLAN.HK.2.9.0.1-02146-QCAHKSWPL_SILICONZ-1
- ath11k: QCA6698AQ hw2.1: add to WLAN.HSP.1.1-04479-QCAHSPSWPL_V1_V2_SILICONZ_IOE-1
- ath11k: QCA6698AQ hw2.1: add board-2.bin
- ath11k: QCA6390 hw2.0: update board-2.bin
- ath11k: QCA2066 hw2.1: update to WLAN.HSP.1.1-03926.13-QCAHSPSWPL_V2_SILICONZ_CE-2.52297.6
- ath11k: QCA2066 hw2.1: update board-2.bin
- ath11k: IPQ8074 hw2.0: update to WLAN.HK.2.9.0.1-02146-QCAHKSWPL_SILICONZ-1
- ath11k: IPQ6018 hw1.0: update to WLAN.HK.2.7.0.1-02409-QCAHKSWPL_SILICONZ-1
- copy-firmware: Fix 'No such file or directory' error.
- ath11k: add device-specific firmware for QCM6490 boards
- qca: add more WCN3950 1.3 NVM files
- qca: add firmware for WCN3950 chips
- qca: move QCA6390 firmware to separate section
- qca: restore licence information for WCN399x firmware
- amdgpu: DMCUB updates for various ASICs
- amdgpu: DMCUB updates forvarious AMDGPU ASICs
- Merge https://github.com/quicjathot/bt_msl_fw_1.1.3_00069 into wcn6750
- qca: Update Bluetooth WCN6750 1.1.0-00476 firmware to 1.1.3-00069
- qcom:x1e80100: Support for Lenovo T14s G6 Qualcomm platform
- qcom:x1e80100: Support for Lenovo T14s G6 Qualcomm platform
- linux-firmware: Update FW files for MRVL SD8997 chips
- i915: Update Xe2LPD DMC to v2.27
- Merge https://github.com/vivesahu-qcom/bt_hsp_fw650 into wcn6856
- qca: Update Bluetooth WCN6856 firmware 2.1.0-00642 to 2.1.0-00650
- rtl_bt: Update RTL8852B BT USB FW to 0x049B_5037
- amdgpu: Update ISP FW for isp v4.1.1
- trivial: contrib: wrap the process in try/except to catch server issues
- trivial: contrib: use python-magic to detect encoding of emails
- Merge https://github.com/che-jiang/qca_btfw into qca
- QCA: Add Bluetooth firmware for QCA6698
- amdgpu: revert DMCUB 3.1.4 firmware
- amlogic: update firmware for w265s2
- mediatek MT7925: update bluetooth firmware to 20250113153307
- linux-firmware: update firmware for MT7925 WiFi device
- amdgpu: update psp 13.0.10 firmware
- amdgpu: update gc 11.0.3 firmware
- amdgpu: update psp 13.0.8 firmware
- amdgpu: update psp 13.0.5 firmware
- amdgpu: update vcn 4.0.0 firmware
- amdgpu: update psp 13.0.0 firmware
- amdgpu: update gc 11.0.0 firmware
- amdgpu: update beige goby firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update navi14 firmware
- amdgpu: update smu 14.0.3 firmware
- amdgpu: update psp 14.0.3 firmware
- amdgpu: update gc 12.0.1 firmware
- amdgpu: update navi12 firmware
- amdgpu: update smu 14.0.2 firmware
- amdgpu: update psp 14.0.2 firmware
- amdgpu: update gc 12.0.0 firmware
- amdgpu: update gc 9.4.3 firmware
- amdgpu: update navi10 firmware
- amdgpu: update vcn 4.0.4 firmware
- amdgpu: update psp 13.0.7 firmware
- amdgpu: update gc 11.0.2 firmware
- amdgpu: update yellow carp firmware
- qcom: correct licence information for SA8775P binaries
- qcom: update SLPI firmware for RB5 board
- amdgpu: DMCUB updates for various AMDGPU ASICs
- qcom: add DSP firmware for SA8775p platform
- qcom: correct venus firmware versions
- qcom: add missing version information
- linux-firmware: Update firmware (v10) for mt7988 internal
- iwlwifi: add Bz FW for core90-93 release
- linux-firmware: wilc3000: add firmware for WILC3000 WiFi device
- rtw89: 8852b: update fw to v0.29.29.8
- rtw89: 8852c: update fw to v0.27.122.0
- rtw89: 8922a: update fw to v0.35.54.0
- rtw89: 8922a: update fw to v0.35.52.1 and stuffs
- rtw89: 8852bt: update fw to v0.29.110.0
- rtw89: 8852b: update fw to v0.29.29.7
- amdgpu: DMCUB updates for various AMDGPU ASICs
- amdgpu: update sdma 6.0.3 firmware
- amdgpu: update psp 13.0.10 firmware
- amdgpu: update gc 11.0.3 firmware
- amdgpu: update sdma 4.4.5 firmware
- amdgpu: update psp 13.0.14 firmware
- amdgpu: update gc 9.4.4 firmware
- amdgpu: update psp 13.0.5 firmware
- amdgpu: update vega20 firmware
- amdgpu: update vega12 firmware
- amdgpu: update vega10 firmware
- amdgpu: update vcn 4.0.0 firmware
- amdgpu: update psp 13.0.0 firmware
- amdgpu: update gc 11.0.0 firmware
- amdgpu: update picasso firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update psp 13.0.11 firmware
- amdgpu: update gc 11.0.4 firmware
- amdgpu: update vcn 4.0.2 firmware
- amdgpu: update psp 13.0.4 firmware
- amdgpu: update gc 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update vcn 4.0.6 firmware
- amdgpu: update psp 14.0.1 firmware
- amdgpu: update vcn 4.0.5 firmware
- amdgpu: update gc 11.5.0 firmware
- amdgpu: update vcn 5.0.0 firmware
- amdgpu: update smu 14.0.3 firmware
- amdgpu: update psp 14.0.3 firmware
- amdgpu: update gc 12.0.1 firmware
- amdgpu: update navi14 firmware
- amdgpu: update arcturus firmware
- amdgpu: update renoir firmware
- amdgpu: update smu 14.0.2 firmware
- amdgpu: update psp 14.0.2 firmware
- amdgpu: update gc 12.0.0 firmware
- amdgpu: update navi12 firmware
- amdgpu: update vcn 4.0.3 firmware
- amdgpu: update sdma 4.4.2 firmware
- amdgpu: update psp 13.0.6 firmware
- amdgpu: update gc 9.4.3 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update vcn 4.0.4 firmware
- amdgpu: update psp 13.0.7 firmware
- amdgpu: update gc 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update aldebaran firmware
- cirrus: cs35l56: Correct some links to address the correct amp instance
- linux-firmware: Update firmware file for Intel Bluetooth Magnetar core
- linux-firmware: Update firmware file for Intel BlazarU core
- linux-firmware: Update firmware file for Intel Bluetooth Solar core
- cirrus: cs35l41: Add Firmware for Ayaneo system 1f660105
Resolves: RHEL-79835

* Wed Jan  8 2025 Denys Vlasenko <dvlasenk@redhat.com> - 20250108-127.gitc0f414a6
- Update linux-firmware to latest upstream (RHEL-73145)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- Fix has_gnu_parallel function
- rtl_bt: Add separate config for RLT8723CS Bluetooth part
- amdgpu: revert VCN 3.1.2 firmware
- amdgpu: revert yellow carp VCN firmware
- amdgpu: revert sienna cichlid VCN firmware
- amdgpu: revert navy flounder VCN firmware
- amdgpu: revert dimgrey cavefish VCN firmware
- WHENCE: Link the Raspberry Pi CM5 and 500 to the 4B
- copy-firmware.sh: Fix typo in error message.
- Add support to install files/symlinks in parallel.
- Makefile: Remove obsolete/broken reference.
- check_whence.py: Use a more portable shebang.
- rtl_bt: Update RTL8852B BT USB FW to 0x04BE_1F5E
- cnm: update chips&media wave521c firmware.
- WHENCE: Add "Info:" tag to text that's clearly not part of the license
- rtl_nic: add firmware rtl8125bp-2
- qcom: venus-5.4: update firmware binary for sc7180 and qcs615
- cirrus: cs35l56: Correct filenames of SSID 17aa3832
- cirrus: cs35l56: Add and update firmware for various Cirrus CS35L54 and CS35L56 laptops
- cirrus: cs35l56: Correct SSID order for 103c8d01 103c8d08 10431f43
- rtl_nic: add firmware rtl8125d-2
- Merge https://github.com/zijun-hu/qca_btfw into qca-bt
- linux-firmware: Update firmware file for Intel BlazarU core
- amdgpu: update dmcub 0.0.246.0 firmware
- Add top level license file.
- amdgpu: update raven firmware
- amdgpu: update gc 11.0.3 firmware
- amdgpu: update psp 13.0.14 firmware
- amdgpu: update vcn 3.1.2 firmware
- amdgpu: update vpe 6.1.3 firmware
- amdgpu: update psp 14.0.4 firmware
- amdgpu: update gc 11.5.2 firmware
- amdgpu: update vcn 4.0.0 firmware
- amdgpu: update gc 11.0.0 firmware
- amdgpu: update picasso firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update gc 11.0.4 firmware
- amdgpu: update green sardine firmware
- amdgpu: update vcn 4.0.2 firmware
- amdgpu: update gc 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update vcn 4.0.6 firmware
- amdgpu: update gc 11.5.1 firmware
- amdgpu: update vcn 4.0.5 firmware
- amdgpu: update psp 14.0.0 firmware
- amdgpu: add vcn 5.0.0 firmware
- amdgpu: add smu 14.0.3 firmware
- amdgpu: add sdma 7.0.1 firmware
- amdgpu: add psp 14.0.3 firmware
- amdgpu: add gc 12.0.1 firmware
- amdgpu: update navi14 firmware
- amdgpu: update renoir firmware
- amdgpu: add smu 14.0.2 firmware
- amdgpu: add sdma 7.0.0 firmware
- amdgpu: add psp 14.0.2 firmware
- amdgpu: add gc 12.0.0 firmware
- amdgpu: update navi12 firmware
- amdgpu: update psp 13.0.6 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update vcn 4.0.4 firmware
- amdgpu: update gc 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update aldebaran firmware
- upstream amdnpu firmware
- QCA: Add Bluetooth nvm files for WCN785x
- i915: Update Xe2LPD DMC to v2.24
- cirrus: cs35l56: Add firmware for Cirrus CS35L56 for various Dell laptops
- iwlwifi: add Bz-gf FW for core89-91 release
- QCA: Update Bluetooth WCN785x firmware to 2.0.0-00515-2
- amdgpu: update smu 13.0.10 firmware
- amdgpu: update sdma 6.0.3 firmware
- amdgpu: update psp 13.0.10 firmware
- amdgpu: update gc 11.0.3 firmware
- amdgpu: add smu 13.0.14 firmware
- amdgpu: add sdma 4.4.5 firmware
- amdgpu: add psp 13.0.14 firmware
- amdgpu: add gc 9.4.4 firmware
- amdgpu: update vcn 3.1.2 firmware
- amdgpu: update psp 13.0.5 firmware
- amdgpu: update psp 13.0.8 firmware
- amdgpu: update vega20 firmware
- amdgpu: update vega12 firmware
- amdgpu: update psp 14.0.4 firmware
- amdgpu: update gc 11.5.2 firmware
- amdgpu: update vega10 firmware
- amdgpu: update vcn 4.0.0 firmware
- amdgpu: update smu 13.0.0 firmware
- amdgpu: update psp 13.0.0 firmware
- amdgpu: update gc 11.0.0 firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update psp 13.0.11 firmware
- amdgpu: update gc 11.0.4 firmware
- amdgpu: update vcn 4.0.2 firmware
- amdgpu: update psp 13.0.4 firmware
- amdgpu: update gc 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update vpe 6.1.1 firmware
- amdgpu: update vcn 4.0.6 firmware
- amdgpu: update psp 14.0.1 firmware
- amdgpu: update gc 11.5.1 firmware
- amdgpu: update vcn 4.0.5 firmware
- amdgpu: update psp 14.0.0 firmware
- amdgpu: update gc 11.5.0 firmware
- amdgpu: update navi14 firmware
- amdgpu: update arcturus firmware
- amdgpu: update renoir firmware
- amdgpu: update navi12 firmware
- amdgpu: update sdma 4.4.2 firmware
- amdgpu: update psp 13.0.6 firmware
- amdgpu: update gc 9.4.3 firmware
- amdgpu: update vcn 4.0.4 firmware
- amdgpu: update psp 13.0.7 firmware
- amdgpu: update gc 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update aldebaran firmware
- ice: update ice DDP wireless_edge package to 1.3.20.0
- ice: update ice DDP comms package to 1.3.52.0
- ice: update ice DDP package to ice-1.3.41.0
- amdgpu: update DMCUB to v9.0.10.0 for DCN314
- amdgpu: update DMCUB to v9.0.10.0 for DCN351
- linux-firmware: Update AMD cpu microcode
- xe: Update GUC to v70.36.0 for BMG, LNL
- i915: Update GUC to v70.36.0 for ADL-P, DG1, DG2, MTL, TGL
Resolves: RHEL-73145

* Tue Nov 19 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20241119-126.git79b5dac1
- Update linux-firmware to latest upstream (RHEL-68279)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- iwlwifi: add Bz-gf FW for core91-69 release
- Merge https://github.com/zijun-hu/qca_btfw into qca
- qcom: venus-5.4: add venus firmware file for qcs615
- qcom: update venus firmware file for SC7280
- QCA: Add 22 bluetooth firmware nvm files for QCA2066
- mediatek MT7922: update bluetooth firmware to 20241106163512
- mediatek MT7921: update bluetooth firmware to 20241106151414
- linux-firmware: update firmware for MT7922 WiFi device
- linux-firmware: update firmware for MT7921 WiFi device
- qcom: Add QDU100 firmware image files.
- qcom: Update aic100 firmware files
- dedup-firmware.sh: fix infinite loop for --verbose
- rtl_bt: Update RTL8852BT/RTL8852BE-VT BT USB FW to 0x04D7_63F7
- cnm: update chips&media wave521c firmware.
- mediatek MT7920: update bluetooth firmware to 20241104091246
- linux-firmware: update firmware for MT7920 WiFi device
- copy-firmware.sh: Run check_whence.py only if in a git repo
- cirrus: cs35l56: Add firmware for Cirrus CS35L56 for various Dell laptops
- amdgpu: update DMCUB to v9.0.10.0 for DCN351
- rtw89: 8852a: update fw to v0.13.36.2
- rtw88: Add firmware v52.14.0 for RTL8812AU
- i915: Update Xe2LPD DMC to v2.23
- linux-firmware: update firmware for mediatek bluetooth chip (MT7925)
- linux-firmware: update firmware for MT7925 WiFi device
- WHENCE: Add sof-tolg for mt8195
- linux-firmware: Update firmware file for Intel BlazarI core
- qcom: Add link for QCS6490 GPU firmware
- qcom: update gpu firmwares for qcs615 chipset
- cirrus: cs35l56: Update firmware for Cirrus Amps for some HP laptops
- ath11k: move WCN6750 firmware to the device-specific subdir
- xe: Update LNL GSC to v104.0.0.1263
- i915: Update MTL/ARL GSC to v102.1.15.1926
- amdgpu: DMCUB updates for various AMDGPU ASICs
- mediatek: Add sof-tolg for mt8195
- i915: Add Xe3LPD DMC
- cnm: update chips&media wave521c firmware.
- linux-firmware: Add firmware for Cirrus CS35L41
- linux-firmware: Update firmware file for Intel BlazarU core
- Makefile: error out of 'install' if COPYOPTS is set
- check_whence.py: skip some validation if git ls-files fails
- qcom: Add Audio firmware for X1E80100 CRD/QCPs
- amdgpu: DMCUB updates forvarious AMDGPU ASICs
- brcm: replace NVRAM for Jetson TX1
- rtlwifi: Update firmware for RTL8192FU to v7.3
- make: separate installation and de-duplication targets
- check_whence.py: check the permissions
- Remove execute bit from firmware files
- configure: remove unused file
- rtl_nic: add firmware rtl8125d-1
- iwlwifi: add gl/Bz FW for core91-69 release
- iwlwifi: update ty/So/Ma firmwares for core91-69 release
- iwlwifi: update cc/Qu/QuZ firmwares for core91-69 release
- Merge https://github.com/zijun-hu/qca_btfw into wcn785x
- cirrus: cs35l56: Add firmware for Cirrus CS35L56 for a Lenovo Laptop
- cirrus: cs35l56: Add firmware for Cirrus CS35L56 for some ASUS laptops
- cirrus: cs35l56: Add firmware for Cirrus Amps for some HP laptops
- linux-firmware: update firmware for en8811h 2.5G ethernet phy
Resolves: RHEL-68279

* Mon Oct 14 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20241014-125.git06bad2f1
- Update linux-firmware to latest upstream (RHEL-62359)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- mtk_wed: add firmware for mt7988 Wireless Ethernet Dispatcher
- ath12k: WCN7850 hw2.0: update board-2.bin
- ath12k: QCN9274 hw2.0: add to WLAN.WBE.1.3.1-00162-QCAHKSWPL_SILICONZ-1
- ath12k: QCN9274 hw2.0: add board-2.bin
- copy-firmware.sh: rename variables in symlink hanlding
- copy-firmware.sh: remove no longer reachable test -L
- copy-firmware.sh: remove no longer reachable test -f
- copy-firmware.sh: call ./check_whence.py before parsing the file
- copy-firmware.sh: warn if the destination folder is not empty
- copy-firmware.sh: add err() helper
- copy-firmware.sh: fix indentation
- copy-firmware.sh: reset and consistently handle destdir
- Revert "copy-firmware: Support additional compressor options"
- copy-firmware.sh: flesh out and fix dedup-firmware.sh
- Style update yaml files
- editorconfig: add initial config file
- check_whence.py: annotate replacement strings as raw
- check_whence.py: LC_ALL=C sort -u the filelist
- check_whence.py: ban link-to-a-link
- check_whence.py: use consistent naming
- Add a link from TAS2XXX1EB3.bin -> ti/tas2781/TAS2XXX1EB30.bin
- tas2781: Upload dsp firmware for ASUS laptop 1EB30 & 1EB31
- rtlwifi: Add firmware v39.0 for RTL8192DU
- Revert "ath12k: WCN7850 hw2.0: update board-2.bin"
- amdgpu: DMCUB DCN35 update
- brcm: Add BCM4354 NVRAM for Jetson TX1
- brcm: Link FriendlyElec NanoPi M4 to AP6356S nvram
- linux-firmware: add firmware for MediaTek Bluetooth chip (MT7920)
- linux-firmware: add firmware for MT7920
- amdgpu: update raven firmware
- amdgpu: update SMU 13.0.10 firmware
- amdgpu: update PSP 13.0.10 firmware
- amdgpu: update GC 11.0.3 firmware
- amdgpu: update VCN 3.1.2 firmware
- amdgpu: update PSP 13.0.5 firmware
- amdgpu: update PSP 13.0.8 firmware
- amdgpu: update vega12 firmware
- amdgpu: update PSP 14.0.4 firmware
- amdgpu: update GC 11.5.2 firmware
- amdgpu: update vega10 firmware
- amdgpu: update VCN 4.0.0 firmware
- amdgpu: update PSP 13.0.0 firmware
- amdgpu: update GC 11.0.0 firmware
- amdgpu: update picasso firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update green sardine firmware
- amdgpu: update VCN 4.0.2 firmware
- amdgpu: update PSP 13.0.4 firmware
- amdgpu: update GC 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update VCN 4.0.6 firmware
- amdgpu: update PSP 14.0.1 firmware
- amdgpu: update GC 11.5.1 firmware
- amdgpu: update VCN 4.0.5 firmware
- amdgpu: update PSP 14.0.0 firmware
- amdgpu: update GC 11.5.0 firmware
- amdgpu: update navi14 firmware
- amdgpu: update renoir firmware
- amdgpu: update navi12 firmware
- amdgpu: update SMU 13.0.6 firmware
- amdgpu: update SDMA 4.4.2 firmware
- amdgpu: update PSP 13.0.6 firmware
- amdgpu: update GC 9.4.3 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update VCN 4.0.4 firmware
- amdgpu: update PSP 13.0.7 firmware
- amdgpu: update GC 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update aldebaran firmware
- qcom: update gpu firmwares for qcm6490 chipset
- mt76: mt7996: add firmware files for mt7992 chipset
- mt76: mt7996: add firmware files for mt7996 chipset variants
- Merge tag 'rtw-fw-2024-09-13' of https://github.com/pkshih/linux-firmware into 8922a
- qcom: add gpu firmwares for sa8775p chipset
- amdgpu: update DMCUB to v0.0.233.0 DCN351
- rtw89: 8922a: add fw format-2 v0.35.42.1
- copy-firmware: Handle links to uncompressed files
- WHENCE: Fix battmgr.jsn entry type
- amdgpu: Add VPE 6.1.3 microcode
- amdgpu: add SDMA 6.1.2 microcode
- amdgpu: Add support for PSP 14.0.4
- amdgpu: add GC 11.5.2 microcode
- qcom: qcm6490: add ADSP and CDSP firmware
- linux-firmware: Update firmware file for Intel Bluetooth Magnetor core
- linux-firmware: Update firmware file for Intel BlazarU core
- linux-firmware: Update firmware file for Intel Bluetooth Solar core
- rtl_bt: Update RTL8852B BT USB FW to 0x0447_9301
- realtek: rt1320: Add patch firmware of MCU
- i915: Update MTL DMC v2.23
- cirrus: cs35l56: Add firmware for Cirrus CS35L54 for some HP laptops
- amdgpu: Revert sienna cichlid dmcub firmware update
- Merge tag 'iwlwifi-fw-2024-09-03' of http://git.kernel.org/pub/scm/linux/kernel/git/iwlwifi/linux-firmware into iwlwifi-20240903
- iwlwifi: add Bz FW for core89-58 release
- rtl_nic: add firmware rtl8126a-3
- linux-firmware: update firmware for MT7921 WiFi device
- linux-firmware: update firmware for mediatek bluetooth chip (MT7921)
- amdgpu: update DMCUB to v0.0.232.0 for DCN314 and DCN351
- qcom: vpu: restore compatibility with kernels before 6.6
Resolves: RHEL-62359

* Tue Aug 27 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20240827-124.git3cff7109
- AMD SEV: IOMMU improperly handles certain special address leading to a loss of guest integrity (RHEL-54256)
- AMD SEV: Incomplete system memory cleanup in SEV firmware corrupt guest private memory (RHEL-54237)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- amdgpu: DMCUB updates forvarious AMDGPU ASICs
- rtw89: 8922a: add fw format-1 v0.35.41.0
- linux-firmware: update firmware for MT7925 WiFi device
- linux-firmware: update firmware for mediatek bluetooth chip (MT7925)
- rtl_bt: Add firmware and config files for RTL8922A
- rtl_bt: Add firmware file for the the RTL8723CS Bluetooth part
- rtl_bt: de-dupe identical config.bin files
- rename rtl8723bs_config-OBDA8723.bin -> rtl_bt/rtl8723bs_config.bin
- linux-firmware: Update AMD SEV firmware
- linux-firmware: update firmware for MT7996
- Revert "i915: Update MTL DMC v2.22"
- Merge tag 'amd-2024-08-12' of https://gitlab.freedesktop.org/drm/firmware into amd-2024-08-12
- ath12k: WCN7850 hw2.0: update board-2.bin
- ath11k: WCN6855 hw2.0: update to WLAN.HSP.1.1-03125-QCAHSPSWPL_V1_V2_SILICONZ_LITE-3.6510.41
- ath11k: WCN6855 hw2.0: update board-2.bin
- ath11k: QCA2066 hw2.1: add to WLAN.HSP.1.1-03926.13-QCAHSPSWPL_V2_SILICONZ_CE-2.52297.3
- ath11k: QCA2066 hw2.1: add board-2.bin
- ath11k: IPQ5018 hw1.0: update to WLAN.HK.2.6.0.1-01291-QCAHKSWPL_SILICONZ-1
- qcom: vpu: add video firmware for sa8775p
- amdgpu: DMCUB updates for various AMDGPU ASICs
- qcom: update path for video firmware for vpu-1/2/3.0
- Merge https://github.com/zijun-hu/qca_btfw into qca_btfw
- Merge tag 'rtw-fw-2024-08-08' of https://github.com/pkshih/linux-firmware into rtw89
- QCA: Update Bluetooth WCN685x 2.1 firmware to 2.1.0-00642
- rtw89: 8852c: add fw format-1 v0.27.97.0
- rtw89: 8852bt: add firmware 0.29.91.0
- amdgpu: Update ISP FW for isp v4.1.1
- Merge tag 'intel-2024-08-02' of https://gitlab.freedesktop.org/drm/firmware into intel-20240805
- Merge https://github.com/zijun-hu/qca_btfw into list-20240802
- mediatek: Update mt8195 SOF firmware
- Merge tag 'amd-2024-08-02' of https://gitlab.freedesktop.org/drm/firmware into amd-20240802
- amdgpu: DMCUB updates for DCN314
- xe: First GuC release v70.29.2 for BMG
- xe: Add GuC v70.29.2 for LNL
- i915: Add GuC v70.29.2 for ADL-P, DG1, DG2, MTL, and TGL
- i915: Update MTL DMC v2.22
- i915: update MTL GSC to v102.0.10.1878
- xe: Add BMG HuC 8.2.10
- xe: Add GSC 104.0.0.1161 for LNL
- xe: Add LNL HuC 9.4.13
- i915: update DG2 HuC to v7.10.16
- amdgpu: Update ISP FW for isp v4.1.1
- amdgpu: Update ISP FW for isp v4.1.1
- amdgpu: add new ISP 4.1.1 firmware
- QCA: Update Bluetooth QCA2066 firmware to 2.1.0-00641
- amdgpu: update DMCUB to v0.0.227.0 for DCN35 and DCN351
- Merge tag 'iwlwifi-fw-2024-07-25' of ssh://gitolite.kernel.org/pub/scm/linux/kernel/git/iwlwifi/linux-firmware into iwlfifi-fw-2024-07
- Revert "iwlwifi: update ty/So/Ma firmwares for core89-58 release"
- linux-firmware: update firmware for MT7922 WiFi device
- linux-firmware: update firmware for MT7921 WiFi device
- linux-firmware: update firmware for mediatek bluetooth chip (MT7922)
- linux-firmware: update firmware for mediatek bluetooth chip (MT7921)
- iwlwifi: add gl FW for core89-58 release
- iwlwifi: update ty/So/Ma firmwares for core89-58 release
- iwlwifi: update cc/Qu/QuZ firmwares for core89-58 release
- mediatek: Update mt8195 SOF firmware and sof-tplg
- ASoC: tas2781: fix the license issue for tas781 firmware
- rtl_bt: Update RTL8852B BT USB FW to 0x048F_4008
- .gitignore: Ignore intermediate files
- i915: Update Xe2LPD DMC to v2.21
Resolves: RHEL-54256, RHEL-54237

* Tue Jul 16 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20240715-123.git4c8fb21e
- [Intel 8.10 FEAT] [SRF] QAT_402XX firmware update (RHEL-47353)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- qcom: move signed x1e80100 signed firmware to the SoC subdir
- qcom: add video firmware file for vpu-3.0
- amdgpu: update DMCUB to v0.0.225.0 for Various AMDGPU Asics
- qcom: add gpu firmwares for x1e80100 chipset
- linux-firmware: add firmware for qat_402xx devices
- amdgpu: update raven firmware
- amdgpu: update SMU 13.0.10 firmware
- amdgpu: update SDMA 6.0.3 firmware
- amdgpu: update PSP 13.0.10 firmware
- amdgpu: update GC 11.0.3 firmware
- amdgpu: update vega20 firmware
- amdgpu: update PSP 13.0.5 firmware
- amdgpu: update PSP 13.0.8 firmware
- amdgpu: update vega12 firmware
- amdgpu: update vega10 firmware
- amdgpu: update VCN 4.0.0 firmware
- amdgpu: update SDMA 6.0.0 firmware
- amdgpu: update PSP 13.0.0 firmware
- amdgpu: update GC 11.0.0 firmware
- amdgpu: update picasso firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update navy flounder firmware
- amdgpu: update PSP 13.0.11 firmware
- amdgpu: update GC 11.0.4 firmware
- amdgpu: update green sardine firmware
- amdgpu: update VCN 4.0.2 firmware
- amdgpu: update SDMA 6.0.1 firmware
- amdgpu: update PSP 13.0.4 firmware
- amdgpu: update GC 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update VPE 6.1.1 firmware
- amdgpu: update VCN 4.0.6 firmware
- amdgpu: update SDMA 6.1.1 firmware
- amdgpu: update PSP 14.0.1 firmware
- amdgpu: update GC 11.5.1 firmware
- amdgpu: update VCN 4.0.5 firmware
- amdgpu: update SDMA 6.1.0 firmware
- amdgpu: update PSP 14.0.0 firmware
- amdgpu: update GC 11.5.0 firmware
- amdgpu: update navi14 firmware
- amdgpu: update renoir firmware
- amdgpu: update navi12 firmware
- amdgpu: update PSP 13.0.6 firmware
- amdgpu: update GC 9.4.3 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update VCN 4.0.4 firmware
- amdgpu: update SMU 13.0.7 firmware
- amdgpu: update SDMA 6.0.2 firmware
- amdgpu: update PSP 13.0.7 firmware
- amdgpu: update GC 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update raven2 firmware
- amdgpu: update aldebaran firmware
- linux-firmware: Update AMD cpu microcode
- intel: avs: Add topology file for I2S Analog Devices 4567
- intel: avs: Add topology file for I2S Nuvoton 8825
- intel: avs: Add topology file for I2S Maxim 98927
- intel: avs: Add topology file for I2S Maxim 98373
- intel: avs: Add topology file for I2S Maxim 98357a
- intel: avs: Add topology file for I2S Dialog 7219
- intel: avs: Add topology file for I2S Realtek 5663
- intel: avs: Add topology file for I2S Realtek 5640
- intel: avs: Add topology file for I2S Realtek 5514
- intel: avs: Add topology file for I2S Realtek 298
- intel: avs: Add topology file for I2S Realtek 286
- intel: avs: Add topology file for I2S Realtek 274
- intel: avs: Add topology file for Digital Microphone Array
- intel: avs: Add topology file for HDMI codecs
- intel: avs: Add topology file for HDAudio codecs
- Add a copy of Apache-2.0
- intel: avs: Update AudioDSP base firmware for APL-based platforms
- linux-firmware: Add ISH firmware file for Intel Lunar Lake platform
- amdgpu: update DMCUB to v0.0.224.0 for Various AMDGPU Asics
- cirrus: cs35l41: Update various firmware for ASUS laptops using CS35L41
- amdgpu: Update ISP FW for isp v4.1.1
- linux-firmware: mediatek: Update MT8173 VPU firmware to v1.2.0
- qcom: Add AIC100 firmware files
- amlogic: Update bluetooth firmware binary
- linux-firmware: Update firmware file for Intel BlazarU core
- linux-firmware: Update firmware file for Intel Bluetooth Magnetor core
- linux-firmware: Update firmware file for Intel Bluetooth Solar core
- linux-firmware: Update firmware file for Intel Bluetooth Pulsar core
- rtl_bt: Update RTL8822C BT UART firmware to 0xB5D6_6DCB
- rtl_bt: Update RTL8822C BT USB firmware to 0xAED6_6DCB
- amdgpu: update DMCUB to v0.0.222.0 for DCN314
- iwlwifi: add ty/So/Ma firmwares for core88-87 release
- iwlwifi: update cc/Qu/QuZ firmwares for core88-87 release
- linux-firmware: add new cc33xx firmware for cc33xx chips
- cirrus: cs35l56: Update firmware for Cirrus CS35L56 for ASUS UM5606 laptop
- cirrus: cs35l56: Update firmware for Cirrus CS35L56 for various ASUS laptops
- Merge https://github.com/zijun-hu/qca_btfw into qca
Resolves: RHEL-47353

* Mon Jun 10 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20240610-122.git90df68d2
- [Intel 8.10 FEAT] [SPR][EMR] QAT firmware update available (RHEL-15607)
- CVE-2023-31346 AMD SEV: Reserved fields in guest message responses may not be zero initialized [rhel-8.10.0] (RHEL-35596)
- amd-ucode early loading broken [rhel-8] (RHEL-16799)
  Changes since the last update are noted on items below, copied from
  the git changelog of upstream linux-firmware repository.
- linux-firmware: Add firmware for Lenovo Thinkbooks
- amdgpu: update yellow carp firmware
- amdgpu: update VCN 4.0.4 firmware
- amdgpu: update SDMA 6.0.2 firmware
- amdgpu: update PSP 13.0.7 firmware
- amdgpu: update GC 11.0.2 firmware
- amdgpu: update navi10 firmware
- amdgpu: update raven2 firmware
- amdgpu: update raven firmware
- amdgpu: update SMU 13.0.10 firmware
- amdgpu: update SDMA 6.0.3 firmware
- amdgpu: update PSP 13.0.10 firmware
- amdgpu: update GC 11.0.3 firmware
- amdgpu: update VCN 3.1.2 firmware
- amdgpu: update PSP 13.0.5 firmware
- amdgpu: update psp 13.0.8 firmware
- amdgpu: update vega20 firmware
- amdgpu: update vega12 firmware
- amdgpu: update vega10 firmware
- amdgpu: update VCN 4.0.0 firmware
- amdgpu: update smu 13.0.0 firmware
- amdgpu: update SDMA 6.0.0 firmware
- amdgpu: update PSP 13.0.0 firmware
- amdgpu: update GC 11.0.0 firmware
- amdgpu: update picasso firmware
- amdgpu: update beige goby firmware
- amdgpu: update vangogh firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update green sardine firmware
- amdgpu: update navy flounder firmware
- amdgpu: update PSP 13.0.11 firmware
- amdgpu: update GC 11.0.4 firmware
- amdgpu: update VCN 4.0.2 firmware
- amdgpu: update SDMA 6.0.1 firmware
- amdgpu: update PSP 13.0.4 firmware
- amdgpu: update GC 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update VCN 4.0.5 firmware
- amdgpu: update PSP 14.0.0 firmware
- amdgpu: update GC 11.5.0 firmware
- amdgpu: update navi14 firmware
- amdgpu: update SMU 13.0.6 firmware
- amdgpu: update PSP 13.0.6 firmware
- amdgpu: update GC 9.4.3 firmware
- amdgpu: update renoir firmware
- amdgpu: update navi12 firmware
- amdgpu: update aldebaran firmware
- amdgpu: add support for PSP 14.0.1
- amdgpu: add support for VPE 6.1.1
- amdgpu: add support for VCN 4.0.6
- amdgpu: add support for SDMA 6.1.1
- amdgpu: add support for GC 11.5.1
- amdgpu: Add support for DCN 3.5.1
- cnm: update chips&media wave521c firmware.
- linux-firmware: Add ordinary firmware for RTL8821AU device
- amdgpu: add new ISP 4.1.1 firmware
- amdgpu: DMCUB updates for various AMDGPU ASICs
- linux-firmware: Amphion: Update vpu firmware
- linux-firmware: Update firmware file for Intel BlazarU core
- linux-firmware: Update firmware file for Intel Bluetooth Magnetor core
- linux-firmware: Update firmware file for Intel Bluetooth Solar core
- linux-firmware: Update firmware file for Intel Bluetooth Solar core
- i915: Add BMG DMC v2.06
- linux-firmware: Add CS35L41 HDA Firmware for Asus HN7306
- linux-firmware: Update firmware tuning for HP Consumer Laptop
- amdgpu: DMCUB updates for various AMDGPU ASICs
- rtl_bt: Update RTL8822C BT UART firmware to 0x0FD6_407B
- rtl_bt: Update RTL8822C BT USB firmware to 0x0ED6_407B
- cirrus: cs35l56: Add firmware for Cirrus CS35L56 for various ASUS laptops
- linux-firmware: Add firmware and tuning for Lenovo Y770S
- amdgpu: DMCUB updates for various AMDGPU ASICs
- linux-firmware: Add firmware for Cirrus CS35L56 for various HP laptops
- i915: Update Xe2LPD DMC to v2.20
- linux-firmware: Remove Calibration Firmware and Tuning for CS35L41
- linux-firmware: Add firmware for Lenovo Thinkbook 13X
- ASoC: tas2781: Add dsp firmware for Thinkpad ICE-1 laptop
- amdgpu: add DMCUB 3.5 firmware
- amdgpu: add VPE 6.1.0 firmware
- amdgpu: add VCN 4.0.5 firmware
- amdgpu: add UMSCH 4.0.0 firmware
- amdgpu: add SDMA 6.1.0 firmware
- amdgpu: add PSP 14.0.0  firmware
- amdgpu: add GC 11.5.0 firmware
- amdgpu: update license date
- Montage: update firmware for Mont-TSSE
- linux-firmware: Add tuning parameter configs for CS35L41 Firmware
- linux-firmware: Fix firmware names for Laptop SSID 104316a3
- linux-firmware: Add CS35L41 HDA Firmware for Lenovo Legion Slim 7 16ARHA7
- linux-firmware: update firmware for mediatek bluetooth chip (MT7922)
- linux-firmware: update firmware for MT7922 WiFi device
- iwlwifi: add gl FW for core87-44 release
- iwlwifi: add ty/So/Ma firmwares for core87-44 release
- iwlwifi: update cc/Qu/QuZ firmwares for core87-44 release
- nvidia: Update Tegra210 XUSB firmware to v50.29
- amdgpu: update beige goby firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update psp 13.0.11 firmware
- amdgpu: update gc 11.0.4 firmware
- amdgpu: update navy flounder firmware
- amdgpu: update renoir firmware
- amdgpu: update vcn 4.0.2 firmware
- amdgpu: update sdma 6.0.1 firmware
- amdgpu: update psp 13.0.4 firmware
- amdgpu: update gc 11.0.1 firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update vega20 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update green sardine firmware
- amdgpu: update vega12 firmware
- amdgpu: update raven2 firmware
- amdgpu: update vcn 4.0.4 firmware
- amdgpu: update smu 13.0.7 firmware
- amdgpu: update sdma 6.0.2 firmware
- amdgpu: update ipsp 13.0.7 firmware
- amdgpu: update gc 11.0.2 firmware
- amdgpu: update vega10 firmware
- amdgpu: update raven firmware
- amdgpu: update navi14 firmware
- amdgpu: update smu 13.0.10 firmware
- amdgpu: update sdma 6.0.3 firmware
- amdgpu: update psp 13.0.10 firmware
- amdgpu: update gc 11.0.3 firmware
- amdgpu: update vcn 3.1.2 firmware
- amdgpu: update psp 13.0.5 firmware
- amdgpu: update gc 10.3.6 firmware
- amdgpu: update navi12 firmware
- amdgpu: update arcturus firmware
- amdgpu: update vangogh firmware
- amdgpu: update navi10 firmware
- amdgpu: update vcn 4.0.3 firmware
- amdgpu: update smu 13.0.6 firmware
- amdgpu: update psp 13.0.6 firmware
- amdgpu: update gc 9.4.3 firmware
- amdgpu: update vcn 4.0.0 firmware
- amdgpu: update smu 13.0.0 firmware
- amdgpu: update sdma 6.0.0 firmware
- amdgpu: update psp 13.0.0 firmware
- amdgpu: update gc 11.0.0 firmware
- amdgpu: update  firmware
- amdgpu: update aldebaran firmware
- amdgpu: update psp 13.0.8 firmware
- amdgpu: update gc 10.3.7 firmware
- linux-firmware: mediatek: Update MT8173 VPU firmware to v1.1.9
- Merge https://github.com/pkshih/linux-firmware into rtw
- ath10k: WCN3990: hw1.0: add qcm2290 firmware API file
- ath10k: WCN3990: hw1.0: move firmware back from qcom/ location
- i915: Add DG2 HuC 7.10.15
- amdgpu: DMCUB updates for various AMDGPU ASICs
- linux-firmware: update firmware for en8811h 2.5G ethernet phy
- mekdiatek: Update mt8186 SOF firmware to v2.0.1
- rtw89: 8852c: update fw to v0.27.56.14
- rtw89: 8922a: add firmware v0.35.18.0
- rtw88: Add RTL8703B firmware v11.0.0
- linux-firmware: Add firmware for Cirrus CS35L56 for Dell laptops
- Montage: update firmware for Mont-TSSE
- WHENCE: Link the Raspberry Pi CM4 and 5B to the 4B
- Intel Bluetooth: Update firmware file for Intel Bluetooth BE200
- Intel Bluetooth: Update firmware file for Magnetor Intel Bluetooth AX101
- Intel Bluetooth: Update firmware file for Magnetor Intel Bluetooth AX203
- Intel Bluetooth: Update firmware file for Magnetor Intel Bluetooth AX211
- Intel Bluetooth: Update firmware file for SolarF Intel Bluetooth AX101
- Intel Bluetooth: Update firmware file for Solar Intel Bluetooth AX101
- Intel Bluetooth: Update firmware file for SolarF Intel Bluetooth AX203
- Intel Bluetooth: Update firmware file for Solar Intel Bluetooth AX203
- Intel Bluetooth: Update firmware file for SolarF Intel Bluetooth AX211
- Intel Bluetooth: Update firmware file for Solar Intel Bluetooth AX211
- Intel Bluetooth: Update firmware file for Solar Intel Bluetooth AX210
- Intel Bluetooth: Update firmware file for Intel Bluetooth AX200
- Intel Bluetooth: Update firmware file for Intel Bluetooth AX201
- Intel Bluetooth: Update firmware file for Intel Bluetooth 9560
- Intel Bluetooth: Update firmware file for Intel Bluetooth 9260
- amdgpu: DMCUB updates for various AMDGPU ASICs
- linux-firmware: mediatek: Update MT8173 VPU firmware to v1.1.8
- imx: sdma: update firmware to v3.6/v4.6
- linux-firmware: update firmware for mediatek bluetooth chip (MT7921)
- iwlwifi: update 9000-family firmwares to core85-89
- rtl_bt: Update RTL8852A BT USB firmware to 0xD9D6_17DA
- linux-firmware: update firmware for MT7921 WiFi device
- linux-firmware: update firmware for mediatek bluetooth chip (MT7922)
- linux-firmware: update firmware for MT7922 WiFi device
- linux-firmware: Add CS35L41 HDA Firmware for Lenovo Thinkbook 16P Laptops
- amdgpu: Update VCN firmware binaries
- Intel IPU2: Add firmware files
- brcm: Add nvram for the Acer Iconia One 7 B1-750 tablet
- i915: Add Xe2LPD DMC v2.18
- i915: Update MTL DMC v2.21
- linux-firmware: update firmware for en8811h 2.5G ethernet phy
- linux-firmware: add firmware for MT7996
- xe: First GuC release for LNL and Xe
- i915: Add GuC v70.20.0 for ADL-P, DG1, DG2, MTL and TGL
- linux-firmware: Add CS35L41 firmware for Lenovo Legion 7i gen7 laptop (16IAX7)
- brcm: Add nvram for the Asus Memo Pad 7 ME176C tablet
- ice: update ice DDP package to 1.3.36.0
- Intel IPU3 ImgU: Move firmware file under intel/ipu
- Intel IPU6: Move firmware binaries under ipu/
- check_whence: Add a check for duplicate link entries
- WHENCE: Clean up section separators
- linux-firmware: Add CS35L41 firmware for additional ASUS Zenbook 2023 models
- panthor: Add initial firmware for Gen10 Arm Mali GPUs
- amdgpu: DMCUB Updates for DCN321: 7.0.38.0
- amdgpu: DMCUB updates for Yellow Carp: 4.0.68.0
- qcom: update venus firmware file for v5.4
- Montage: add firmware for Mont-TSSE
- amdgpu: update DMCUB to v0.0.203.0 for DCN314 and DCN32
- linux-firmware: Remove 2 HP laptops using CS35L41 Audio Firmware
- linux-firmware: Fix filenames for some CS35L41 firmwares for HP
- linux-firmware: wilc1000: update WILC1000 firmware to v16.1.2
- rtl_nic: add firmware for RTL8126A
- linux-firmware: intel: Add IPU6 firmware binaries
- ath11k: WCN6855 hw2.0: update to WLAN.HSP.1.1-03125-QCAHSPSWPL_V1_V2_SILICONZ_LITE-3.6510.37
- qcom: Add Audio firmware for SM8550 HDK
- Merge tag 'amd-2024-01-30.2' into mlimonci/amd-2024-01-30.2
- Revert "amdgpu: DMCUB updates for various AMDGPU ASICs"
- amdgpu: update SMU 13.0.0 firmware
- amdgpu: update PSP 13.0.0 firmware
- amdgpu: update GC 11.0.0 firmware
- brcm: Add brcmfmac43430-sdio.xxx.txt nvram for the Chuwi Hi8 (CWI509) tablet
- amdgpu: DMCUB updates for various AMDGPU ASICs
- qcom: Add Audio firmware for SM8650 MTP
- linux-firmware: Add firmware for Cirrus CS35L41 on HP Consumer Laptops
- Intel Bluetooth: Make spacing consistent with rest of WHENCE
- amdgpu: update raven2 firmware
- amdgpu: update raven firmware
- amdgpu: update SDMA 5.2.7 firmware
- amdgpu: update PSP 13.0.8 firmware
- amdgpu: update VCN 3.1.2 firmware
- amdgpu: update SDMA 5.2.6 firmware
- amdgpu: update PSP 13.0.5 firmware
- amdgpu: update GC 10.3.6 firmware
- amdgpu: add GC 11.0.1 rlc_1 firmware
- amdgpu: update vega20 firmware
- amdgpu: update VCN 4.0.0 firmware
- amdgpu: update SMU 13.0.0 firmware
- amdgpu: update PSP 13.0.0 firmware
- amdgpu: update GC 11.0.0 firmware
- amdgpu: update vega12 firmware
- amdgpu: update vega10 firmware
- amdgpu: update beige goby firmware
- amdgpu: update picasso firmware
- amdgpu: update dimgrey cavefish firmware
- amdgpu: update vangogh firmware
- amdgpu: update navy flounder firmware
- amdgpu: update green sardine firmware
- amdgpu: update sienna cichlid firmware
- amdgpu: update PSP 13.0.11 firmware
- amdgpu: update GC 11.0.4 firmware
- amdgpu: update VCN 4.0.2 firmware
- amdgpu: update PSP 13.0.4 firmware
- amdgpu: update GC 11.0.1 firmware
- amdgpu: update arcturus firmware
- amdgpu: update navi14 firmware
- amdgpu: add VCN 4.0.3 firmware
- amdgpu: add SDMA 4.4.2 firmware
- amdgpu: add SMU 13.0.6 firmware
- amdgpu: add PSP 13.0.6 firmware
- amdgpu: Add GC 9.4.3 firmware
- amdgpu: update renoir firmware
- amdgpu: update VCN 4.0.4 firmware
- amdgpu: update SMU 13.0.7 firmware
- amdgpu: update PSP 13.0.7 firmware
- amdgpu: update GC 11.0.2 firmware
- amdgpu: update navi12 firmware
- amdgpu: update yellow carp firmware
- amdgpu: update SMU 13.0.10 firmware
- amdgpu: update SDMA 6.0.3 firmware
- amdgpu: update PSP 13.0.10 firmware
- amdgpu: update GC 11.0.3 firmware
- amdgpu: update navi10 firmware
- amdgpu: update aldebaran firmware
- linux-firmware: Update AMD cpu microcode
- RTL8192E: Remove old realtek WiFi firmware
- Intel Bluetooth: Update firmware file for Magnetor Intel Bluetooth AX101
- Intel Bluetooth: Update firmware file for Magnetor Intel Bluetooth AX203
- Intel Bluetooth: Update firmware file for SolarF Intel Bluetooth AX203
- Intel Bluetooth: Update firmware file for SolarF Intel Bluetooth AX211
- Intel Bluetooth: Update firmware file for Solar Intel Bluetooth AX211
- amdgpu: DMCUB updates for DCN314
- qcom: Update the firmware for Adreno a630 family of GPUs
- cirrus: Add CS35L41 firmware for Legion Slim 7 Gen 8 laptops
- linux-firmware: Add firmware for Cirrus CS35L41 for various Dell laptops
- linux-firmware: update firmware for qat_4xxx devices
Resolves: RHEL-35596, RHEL-16799, RHEL-15607

* Thu Jan 11 2024 Denys Vlasenko <dvlasenk@redhat.com> - 20240111-121.gitb3132c18
- Pass --ignore-duplicates to copy-firmware.sh
- AMD Zen3 and Zen4: fix for INVD instruction causing loss of SEV-ES guest machine memory integrity
Resolves: RHEL-13982

* Wed Nov 22 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20231121-120.git9552083a
- Work around absense of rdfind during build
- Add file directives for new iwlwifi files
Resolves: RHEL-16721, RHEL-14260

* Tue Nov 21 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20231121-119.git9552083a
- Update to latest upstream linux-firmware image for assorted updates
- Update AMD cpu microcode
- hw: intel: Fix protection mechanism failure for some Intel(R) PROSet/Wireless WiFi
Resolves: RHEL-16721, RHEL-14260

* Thu Aug 24 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20230824-118.git0e048b06
- Update to latest upstream linux-firmware image for assorted updates
- AMD Zen3 and Zen4 firmware update for return address predictor velunerability
Resolves: rhbz#2230415

* Tue Aug 08 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20230808-117.git0ab353f8
- Update to latest upstream linux-firmware image for assorted updates
- Navi32 dGPU firmware
- Update to fix multi monitor behind TBT3 dock & random flickers
- AMD Zen2 firmware update for cross-process information leak
Resolves: rhbz#2047482, rhbz#2227846, rhbz#2227153

* Tue Jul 11 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20230711-116.gitd3f66064
- Update to latest upstream linux-firmware image for assorted updates
- AMD GPU firmware update: fix PSR-SU issues with kernel 6.2 or later
Resolves: rhbz#2218670

* Mon May 15 2023 Denys Vlasenko <dvlasenk@redhat.com> - 20230515-115.gitd1962891
- Update to latest upstream linux-firmware image for assorted updates
- [RHEL8] Add latest NVIDIA signed firmware for Turing GPUs and later
Resolves: rhbz#2183606

* Wed Apr 05 2023 Patrick Talbert <ptalbert@redhat.com> - 20230404-114.git2e92a49f
- Update to latest upstream linux-firmware image for assorted updates
- Intel QAT Update - firmware for QAT (rhbz 2030316)

* Fri Feb 17 2023 Lucas Zampieri <lzampier@redhat.com> - 20230217-113.git83f1d778
- Update to latest upstream linux-firmware image for assorted updates
- Qualcomm Wi-Fi - Disconnecting power cable during suspend wakes up the laptop (rhbz 2169012)

* Wed Jan 11 2023 Lucas Zampieri <lzampier@redhat.com> - 20230111-112.gita1ad1d5b
- Update to latest upstream linux-firmware image for assorted updates
- Navi33 dGPU firmware (rhbz 2047480)
- Navi31 dGPU firmware (rhbz 2047483)

* Wed Nov 02 2022 Lucas Zampieri <lzampier@redhat.com> - 20221102-111.git8bb75626
- Update to latest upstream linux-firmware image for assorted updates
- amdgpu firmware update to fix certain "multiple monitor scenarios" (rhbz 2130707)

* Tue Jul 26 2022 Jarod Wilson <jarod@redhat.com> - 20220726-110.git150864a4
- Omit unused password-protected vxge firmware files from package (rhbz 2108051)
- Pick up latest AMD GPU firmwares

* Wed Jul 13 2022 Jarod Wilson <jarod@redhat.com> - 20220713-109.gitdfa29317
- Update to latest upstream linux-firmware image for assorted updates
- Include even newer qed firmware update (rhbz 2040269)

* Fri Jun 03 2022 Jarod Wilson <jarod@redhat.com> - 20220517-108.git251d2900
- Bump NVR due to conflict with late 8.6 iwl firmware package versioning

* Wed May 18 2022 Jarod Wilson <jarod@redhat.com> - 20220517-107.git251d2900
- Update to latest upstream linux-firmware image for assorted updates
- Include latest qed firmware update (rhbz 2040269)
- Include latest bnx2x firmware update (rhbz 2040273)
- Include latest ice firmware update (rhbz 2059384, rhbz 2081543)
- Include latest Qualcomm firmware update (rhbz 2062871)

* Thu Feb 10 2022 Augusto Caringi <acaringi@redhat.com> - 20220209-106.git6342082c
- Update to latest upstream linux-firmware image for assorted updates
- Include AMD GPU firmware fix (rhbz 2031172)
- Support QCA WCN6856 v2.1 Module: Bluetooth firmware (rhbz 2007904)

* Mon Dec 06 2021 Bruno Meneguele <bmeneg@redhat.com> - 20211119-105.gitf5d51956
- Update to latest upstream linux-firmware image for assorted updates
- Update to latest AMD GPU firmware (rhbz 1986660)

* Thu Oct 07 2021 Bruno Meneguele <bmeneg@redhat.com> - 20211007-104.git7a300505
- Update to latest upstream linux-firmware image for assorted updates
- Certain amdgpu firmware files cause random hangs with AMD Picasso/Raven Ridge APUs (rhbz 2000879)
- Support Intel CNVi AX211 Garfield Peak2 on ADL - Bluetooth (rhbz 1923179)
- Support Intel CNVi AX211 Garfield Peak2 on ADL - WIFI (rhbz 1923175)

* Fri Jul 02 2021 Bruno Meneguele <bmeneg@redhat.com> - 20210702-103.gitd79c2677
- Update to latest upstream linux-firmware image for assorted updates
- Include support for the Qualcomm AX500-DBS (QCA6390) 802.11ax Wireless (rhbz 1725913)
- Firmware for Chelsio T4/T5/T6 adapters (rhbz 1961398)
- Incorrect NVIDIA GPU firmware used for TU102 and TU104 GPUs (rhbz 1965312)
- Package the new iwl7260 ucode files and also the new binary format PNVM

* Wed Jan 27 2021 Jan Stancek <jstancek@redhat.com> - 20201218-102.git05789708
- Update to latest upstream linux-firmware image for assorted updates (rhbz 1918613)

* Fri Dec 04 2020 Jan Stancek <jstancek@redhat.com> - 20201118-101.git7455a360
- Update to latest upstream linux-firmware image for assorted updates
- Include to support Intel AX210 Typhoon Peak - Bluetooth firmware support (rhbz 1897069)
- Include support for the Qualcomm AX500-DBS (QCA6390) 802.11ax Wireless - firmware support (rhbz 1725913)

* Thu Nov 05 2020 Jan Stancek <jstancek@redhat.com> - 20201022-100.gitdae4b4cd
- Update to latest upstream linux-firmware image for assorted updates
- ice: Pull Comms Market Segment Package (rhbz 1861491)
- ice: Update to the Default OS DDP Package for ice driver (rhbz 1879278)

* Fri Jun 19 2020 Frantisek Hrbata <fhrbata@redhat.com> - 20200619-99.git3890db36
- Update to latest upstream linux-firmware image for assorted updates (rhbz 1847992)

* Tue May 12 2020 Frantisek Hrbata <fhrbata@redhat.com> - 20200512-98.gitb2cad6a2
- Update to latest upstream linux-firmware image for assorted updates
- Update qed zipped/unzipped firmware to latest upstream (rhbz 1791030)
- Update bnx2x firmware to latest upstream (rhbz 1791031)
- Firmware - nvidia: add TU102/TU104/TU106 / TU116/117 signed firmware (rhbz 1801025)
- Update latest Signed FW for AMD NAVI 10 Graphic (rhbz 1809846)

* Thu Dec 12 2019 Bruno Meneguele <bmeneg@redhat.com> - 20191202-97.gite8a0f4c9
- firmware files marked as configuration files (rhbz 1782329)

* Mon Dec 02 2019 Bruno Meneguele <bmeneg@redhat.com> - 20191202-96.gite8a0f4c9
- Fix .files file dir, wrongly modified in the last release

* Mon Dec 02 2019 Bruno Meneguele <bmeneg@redhat.com> - 20191202-95.gite8a0f4c9
- Update to latest upstream linux-firmware image for assorted updates
- Update qed zipped/unzipped firmware to latest upstream (rhbz 1720394)
- Update bnx2x firmware to latest upstream (rhbz 1720395)
- AX200(Cyclone Peak 2 or CcP2) Bluetooth support (rhbz 1722693)
- Firmware for Chelsio T4/T5/T6 adapters (rhbz 1725821)
- Include to support Intel AX201 Harrison Peak(CNVi) on CML-U (rhbz 1725873)
- Icelake Bluetooth firmware update (AX201, Harrison Peak, or HrP2) (rhbz 1726182)
- DDP Package to support Columbiaville NIC (rhbz 1726454)
- Add iwlwifi-cc-a0-48.ucode to linux-firmware (rhbz 1753962)
- Change install section to use usptream installation process

* Thu May 16 2019 Herton R. Krzesinski <herton@redhat.com> - 20190516-94.git711d3297
- Update to latest upstream linux-firmware image for assorted updates
- Update iwlwifi firmware to support the Cyclone Peak Dual Band 2x2 802.11ax (rhbz 1622442)
- Pull new version of Netronome firmware (rhbz 1663989)
- Update firmware for Chelsio T4/T5/T6 adapters (rhbz 1664678)
- Update Netronome firmware that include bug fixes (rhbz 1673890)
- Include firmware support for Intel Bluetooth 22261 (rhbz 1678095)

* Fri Mar 29 2019 Herton R. Krzesinski <herton@redhat.com> - 20190111-93.gitd9fb2ee6
- Remove cxgb3 (T3 adapter) firmware (rhbz 1503721)

* Fri Jan 11 2019 Herton R. Krzesinski <herton@redhat.com> - 20190111-92.gitd9fb2ee6
- Update linux-firmware package for bugfixes for Netronome (rhbz 1657892)

* Tue Nov 20 2018 Herton R. Krzesinski <herton@redhat.com> - 20181109-91.git1baa3486
- Make netronome/nic_AMDA* symbolic links be config noreplace files (Pablo Cascón) [1644273]

* Fri Nov 09 2018 Herton R. Krzesinski <herton@redhat.com> - 20181109-90.git1baa3486
- Update to latest upstream linux-firmware image for assorted updates
- Include BPF version of Netronome firmware (rhbz 1637558)

* Mon Oct 15 2018 Herton R. Krzesinski <herton@redhat.com> - 20181015-89.gitc6b6265d
- Remove liquidio/lio_23xx_vsw.bin from linux-firmware until GPL violation is
  fully resolved (rhbz 1637694)

* Thu Oct 11 2018 Herton R. Krzesinski <herton@redhat.com> - 20181011-88.gitc6b6265d
- Update to latest upstream linux-firmware image for assorted updates
- Include latest firmware for Nvidia Graphics (rhbz 1504667)

* Tue Jul 17 2018 Herton R. Krzesinski <herton@redhat.com> - 20180717-87.git8d69bab7
- Update to latest upstream linux-firmware image for assorted updates
- cxgb4: update firmware to revision 1.20.8.0 (rhbz 1503622)

* Fri Jul 06 2018 Herton R. Krzesinski <herton@redhat.com> - 20180706-86.gitd1147327
- Update to latest upstream linux-firmware image for assorted updates
- qed: Add firmware 8.37.2.0 (rhbz 1584853)

* Mon Jun 11 2018 Herton R. Krzesinski <herton@redhat.com> - 20180524-85.git2a9b2cf5
- Add Obsoletes for conflicting ivtv-firmware versions (rhbz 1589055)
- Add Obsoletes for iwl7265-firmware which is shipped with RHEL 7 (rhbz 1589056)

* Thu May 24 2018 Herton R. Krzesinski <herton@redhat.com> - 20180524-84.git2a9b2cf5
- Update to latest upstream linux-firmware image for assorted updates
- Update Intel Omni-Path Architecture (OPA) Firmware (rhbz 1483737)
- Drop separate microcode_amd_fam17h.bin, same file is now included in linux-firmware
  upstream

* Mon Apr 02 2018 Josh Boyer <jwboyer@fedoraproject.org> - 20180402-83.git8c1e439c
- Latest upstream snapshot

* Fri Feb 09 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 20171215-82.git2451bb22.1
- Escape macros in %%changelog

* Fri Jan 05 2018 Josh Boyer <jwboyer@fedoraproject.org> 20171215-92.git2451bb22
- Add amd-ucode for fam17h

* Fri Dec 15 2017 Josh Boyer <jwboyer@fedoraproject.org> 20171215-81.git2451bb22
- Updated skl DMC, cnl audio, netronome SmartNIC, amdgpu vega10 and raven,
  intel bluetooth, brcm CYW4373, and liquidio vswitch firmwares

* Sun Nov 26 2017 Josh Boyer <jwboyer@fedoraproject.org> 20171126-80.git17e62881
- Updated bcm 4339 4354 4356 4358 firmware, new bcm 43430
- Fixes CVE-2016-0801 CVE-2017-0561 CVE-2017-9417

* Thu Nov 23 2017 Peter Robinson <pbrobinson@fedoraproject.org> 20171123-79.git90436ce
- Updated Intel GPU, amdgpu, iwlwifi, mvebu wifi, liquidio, QCom a530 & Venus, mlxsw, qed
- Add iwlwifi 9000 series

* Wed Oct 11 2017 Peter Robinson <pbrobinson@fedoraproject.org> 20171009-78.gitbf04291
- Updated cxgb4, QCom gpu, Intel OPA IB, amdgpu, rtlwifi
- Ship the license in %%license for all sub packages
- Modernise spec

* Mon Sep 18 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170828-77.gitb78acc9
- Add patches to fix ath10k regression (rhbz 1492161)

* Mon Aug 28 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170828-76.gitb78acc9
- Update to latest upstream snapshot
- ath10k, iwlwifi, kabylake, liquidio, amdgpu, and cavium crypot updates

* Thu Jun 22 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170622-75.gita3a26af2
- Update to latest upstream snapshot
- imx, qcom, and tegra ARM related updates

* Mon Jun 05 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170605-74.git37857004
- Update to latest upstream snapshot

* Wed Apr 19 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170419-73.gitb1413458
- Update to the latest upstream snapshot
- New nvidia, netronome, and marvell firmware
- Updated intel audio firmware

* Mon Mar 13 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170313-72.git695f2d6d
- Update to the latest upstream snapshot
- New nvidia, AMD, and i915 GPU firmware
- Updated iwlwifi and intel bluetooth firmware

* Mon Feb 13 2017 Josh Boyer <jwboyer@fedoraproject.org> - 20170213-71.git6d3bc888
- Update to the latest upstream snapshot

* Wed Feb 01 2017 Stephen Gallagher <sgallagh@redhat.com> - 20161205-70.git91ddce49
- Add missing %%license macro

* Mon Dec 05 2016 Josh Boyer <jwboyer@fedoraproject.org> 20161205-69.git91ddce49
- Update to the latest upstream snapshot
- New intel bluetooth and rtlwifi firmware

* Fri Sep 23 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160923-68.git42ad5367
- Update to the latest upstream snapshot
- ath10k, amdgpu, mediatek, brcm, marvell updates

* Tue Aug 16 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160816-67.git7c3dfc0b
- Update to the latest upstream snapshot (rhbz 1367203)
- Intel audio, rockchip, amdgpu, iwlwifi, nvidia pascal updates

* Thu Jun 09 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160609-66.gita4bbc811
- Update to the latest upstream snapshot
- Intel bluetooth, radeon smc, Intel braswell/broxton audio, cxgb4 updates

* Thu May 26 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160526-65.git80d463be
- Update to the latest upstream snapshot
- amdgpu, Skylake audio, and rt2xxx wifi firmware updates

* Thu May 05 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160505-64.git8afadbe5
- Update to the latest upstream snapshot
- AMD, intel, and QCA6xxx updates (rhbz 1294263)

* Mon Mar 21 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160321-63.git5f8ca0c
- Update to latest upstream snapshot
- New Skylake GuC and audio firmware, AMD ucode updates

* Wed Mar 16 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160316-62.gitdeb1d836
- Update to latest upstream snapshot
- New firmware for iwlwifi 3168, 7265D, 8000C, and 8265 devices

* Thu Feb 04 2016 Josh Boyer <jwboyer@fedoraproject.org> 20160204-61.git91d5dd13
- Update to latest upstream snashot
- rtlwifi, iwlwifi, intel bluetooth, skylake audio updates

* Thu Feb 04 2016 Fedora Release Engineering <releng@fedoraproject.org> - 20151214-60.gitbbe4917c.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Mon Dec 14 2015 Josh Boyer <jwboyer@fedoraproject.org> 20151214-60.gitbbe4917c
- Update to latest upstream snapshot
- Includes firmware for mt7601u (rhbz 1264631)

* Mon Nov 30 2015 Josh Boyer <jwboyer@fedoraproject.org> 20151130-59.gita109a8ff
- Update to latest upstream snapshot
- Includes -16 ucode for iwlwifi, skylake dmc and audio updates, brcm updates
  bnx2x, and others

* Fri Oct 30 2015 Josh Boyer <jwboyer@fedoraproject.org> 20151030-58.git66d3d8d7
- Update to latest upstream snapshot
- Includes ath10k and mwlwifi firmware updates (rhbz 1276360)

* Mon Oct 12 2015 Josh Boyer <jwboyer@fedoraproject.org> 20151012-57.gitd82d3c1e
- Update to latest upstream snapshot
- Includes skylake and intel bluetooth firmware updates

* Fri Sep 04 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150904-56.git6ebf5d57
- Update to latest upstream git snapshot
- Includes amdgpu firmware and skylake updates

* Thu Sep 03 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150903-55.git38358cfc
- Add firmware from Alex Deucher for amdgpu driver (rhbz 1259542)

* Thu Sep 03 2015 Josh Boyer <jwboyer@fedoraproject.org>
- Update to latest upstream git snapshot
- Updates for nvidia, bnx2x, and atmel devices

* Wed Jul 15 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150715-54.git69640304
- Update to latest upstream git snapshot
- New iwlwifi firmware for 726x/316x/8000 devices
- New firmware for i915 skylake and radeon devices
- Various other updates

* Tue Jun 23 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150521-53.git3161bfa4
- Don't obsolete ivtv-firmware any longer (rhbz 1232773)

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20150521-52.git3161bfa4.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Thu May 21 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150521-52.git3161bfa4
- Update to latest upstream git snapshot
- Updated iwlwifi 316x/726x firmware
- Add cx18-firmware Obsoletes from David Ward (rhbz 1222164)

* Wed May 06 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150415-51.gitec89525b
- Obsoletes ivtv-firmware (rbhz 1211055)

* Fri May 01 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150415-50.gitec89525b
- Add v4l-cx25840.fw back now that ivtv-firmware is retired (rhbz 1211055)

* Tue Apr 14 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150415-49.gitec89525b
- Fix conflict with ivtv-firmware (rhbz 1203385)

* Fri Apr 10 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150415-47.gitec89525b
- Update to the latest upstream git snapshot

* Thu Mar 19 2015 Josh Boyer <jwboyer@fedoraproject.org>
- Ship the cx18x firmware files (rhbz 1203385)

* Mon Mar 16 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150316-46.git020e534e
- Update to latest upstream git snapshot

* Fri Feb 13 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150213-45.git17657c35
- Update to latest upstream git snapshot
- Firmware for Surface Pro 3 WLAN/Bluetooth (rhbz 1185804)

* Thu Jan 15 2015 Josh Boyer <jwboyer@fedoraproject.org> 20150115-44.git78535e88.fc22
- Update to latest upstream git snapshot
- Adjust iwl{3160,7260} version numbers (rhbz 1167695)

* Tue Oct 14 2014 Josh Boyer <jwboyer@fedoraproject.org> 20141013-43.git0e5f6377.fc22
- Fix 3160/7260 version numbers (rhbz 1110522)

* Mon Oct 13 2014 Josh Boyer <jwboyer@fedoraproject.org> 20141013-42.git0e5f6377.fc22
- Update to latest upstream git snapshot

* Fri Sep 12 2014 Josh Boyer <jwboyer@fedoraproject.org> 20140912-41.git365e80cce.fc22
- Update to the latest upstream git snapshot

* Thu Aug 28 2014 Josh Boyer <jwboyer@fedoraproject.org>
- Update to latest upstream git snapshot for new radeon firmware (rhbz 1130738)
- Fix versioning after mass rebuild and for iwl5000-firmware (rhbz 1130979)

* Fri Aug 08 2014 Kyle McMartin <kyle@fedoraproject.org> 20140808-39.gitce64fa89.1
- Update from upstream linux-firmware.
- Nuke unapplied radeon patches.

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20140605-38.gita4f3bc03.1
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Thu Jun 05 2014 Josh Boyer <jwboyer@fedoraproject.org> - 20140605-38.gita4f3bc03
- Updates for Intel 3160/7260/7265 firmware (1087717)
- Add firmware for rtl8723be (rhbz 1091753)
- Updates for radeon CIK, SI/CI, and Mullins/Beema GPUs (rhbz 1094153)
- Various other firmware updates

* Mon Mar 17 2014 Josh Boyer <jwboyer@fedoraproject.org>
- Updates for Intel 3160/7260 and BCM43362 (rhbz 1071590)

* Tue Mar 04 2014 Josh Boyer <jwboyer@fedoraproject.org>
- Fixup Intel wireless package descriptions and Source0 (rhbz 1070600)

* Fri Jan 31 2014 Josh Boyer <jwboyer@fedoraproject.org> - 20140131-35.gitd7f8a7c8
- Update to new snapshot
- Updates for Intel 3160/7260, radeon HAWAII GPUs, and some rtlwifi chips
- Fixes bugs 815579 1046935

* Tue Oct 01 2013 Kyle McMartin <kyle@fedoraproject.org> - 20131001-32.gitb8ac7c7e
- Update to a new git snapshot, drop radeon patches.

* Mon Sep 16 2013 Josh Boyer <jwboyer@fedoraproject.org> - 20130724-31.git31f6b30
- Obsolete ql2x00-firmware packages again (rhbz 864959)

* Sat Jul 27 2013 Josh Boyer <jwboyer@redhat.com> - 20130724-30.git31f6b30
- Add AMD ucode back in now that microcode_ctl doesn't provide it

* Fri Jul 26 2013 Dave Airlie <airlied@redhat.com> 20130724-29.git31f6b30
- add radeon firmware which are lost on the way upstream (#988268)

* Thu Jul 25 2013 Josh Boyer <jwboyer@redhat.com> - 20130724-28.git31f6b30
- Temporarily remove AMD microcode (rhbz 988263)
- Remove Creative CA0132 HD-audio files as they're in alsa-firmware

* Wed Jul 24 2013 Josh Boyer <jwboyer@redhat.com> - 20130724-27.git31f6b30
- Update to latest upstream
- New rtl, iwl, and amd firmware

* Fri Jun 07 2013 Josh Boyer <jwboyer@redhat.com> - 20130607-26.git2892af0
- Update to latest upstream release
- New radeon, bluetooth, rtl, and wl1xxx firmware

* Mon May 20 2013 Kyle McMartin <kyle@redhat.com> - 20130418-25.gitb584174
- Use a common version number for both the iwl*-firmware packages and
  linux-firmware itself.
- Don't reference old kernel-firmware package in %%description

* Mon May 20 2013 Kyle McMartin <kyle@redhat.com> - 20130418-0.3.gitb584174
- Bump iwl* version numbers as well...

* Mon May 20 2013 Kyle McMartin <kyle@redhat.com> - 20130418-0.2.gitb584174
- UsrMove: move firmware to /usr/lib/firmware
- Remove duplicate /usr/lib/firmware/updates entry (already in linux-firmware.dirs)
- Simplify sed by using '!' instead of '/' as regexp delimiter
- Fix date error (commited on Mon Feb 04, so change that entry)

* Thu Apr 18 2013 Josh Boyer <jwboyer@redhat.com> - 20130418-0.1.gitb584174
- Update to latest upstream git tree

* Tue Mar 19 2013 Josh Boyer <jwboyer@redhat.com>
- Own the firmware directories (rhbz 919249)

* Thu Feb 21 2013 Josh Boyer <jwboyer@redhat.com> - 20130201-0.4.git65a5163
- Obsolete netxen-firmware.  Again.  (rhbz 913680)

* Mon Feb 04 2013 Josh Boyer <jwboyer@redhat.com> - 20130201-0.3.git65a5163
- Obsolete ql2[45]00-firmware packages (rhbz 906898)

* Fri Feb 01 2013 Josh Boyer <jwboyer@redhat.com>
- Update to latest upstream release
- Provide firmware for carl9170 (rhbz 866051)

* Wed Jan 23 2013 Ville Skyttä <ville.skytta@iki.fi> - 20121218-0.2.gitbda53ca
- Own subdirs created in /lib/firmware (rhbz 902005)

* Wed Jan 23 2013 Josh Boyer <jwboyer@redhat.com>
- Correctly obsolete the libertas-usb8388-firmware packages (rhbz 902265)

* Tue Dec 18 2012 Josh Boyer <jwboyer@redhat.com>
- Update to latest upstream.  Adds brcm firmware updates

* Wed Oct 10 2012 Josh Boyer <jwboyer@redhat.com>
- Consolidate rt61pci-firmware and rt73usb-firmware packages (rhbz 864959)
- Consolidate netxen-firmware and ql2[123]xx-firmware packages (rhbz 864959)

* Tue Sep 25 2012 Josh Boyer <jwboyer@redhat.com>
- Update to latest upstream.  Adds marvell wifi updates (rhbz 858388)

* Tue Sep 18 2012 Josh Boyer <jwboyer@redhat.com>
- Add patch to create libertas subpackages from Daniel Drake (rhbz 853198)

* Fri Sep 07 2012 Josh Boyer <jwboyer@redhat.com> 20120720-0.2.git7560108
- Add epoch to iwl1000 subpackage to preserve upgrade patch (rhbz 855426)

* Fri Jul 20 2012 Josh Boyer <jwboyer@redhat.com> 20120720-0.1.git7560108
- Update to latest upstream.  Adds more realtek firmware and bcm4334

* Tue Jul 17 2012 Josh Boyer <jwboyer@redhat.com> 20120717-0.1.gitf1f86bb
- Update to latest upstream.  Adds updated realtek firmware

* Thu Jun 07 2012 Josh Boyer <jwboyer@redhat.com> 20120510-0.5.git375e954
- Bump release to get around koji

* Thu Jun 07 2012 Josh Boyer <jwboyer@redhat.com> 20120510-0.4.git375e954
- Drop udev requires.  Systemd now provides udev

* Tue Jun 05 2012 Josh Boyer <jwboyer@redhat.com> 20120510-0.3.git375e954
- Fix location of BuildRequires so git is inclued in the buildroot
- Create iwlXXXX-firmware subpackages (rhbz 828050)

* Thu May 10 2012 Josh Boyer <jwboyer@redhat.com> 20120510-0.1.git375e954
- Update to latest upstream.  Adds new bnx2x and radeon firmware

* Wed Apr 18 2012 Josh Boyer <jwboyer@redhat.com> 20120418-0.1.git85fbcaa
- Update to latest upstream.  Adds new rtl and ath firmware

* Wed Mar 21 2012 Dave Airlie <airlied@redhat.com> 20120206-0.3.git06c8f81
- use git to apply the radeon firmware

* Wed Mar 21 2012 Dave Airlie <airlied@redhat.com> 20120206-0.2.git06c8f81
- add radeon southern islands/trinity firmware

* Tue Feb 07 2012 Josh Boyer <jwboyer@redhat.com> 20120206-0.1.git06c8f81
- Update to latest upstream git snapshot.  Fixes rhbz 786937

* Fri Jan 13 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20110731-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_17_Mass_Rebuild

* Thu Aug 04 2011 Tom Callaway <spot@fedoraproject.org> 20110731-2
- resolve conflict with netxen-firmware

* Wed Aug 03 2011 David Woodhouse <dwmw2@infradead.org> 20110731-1
- Latest firmware release with v1.3 ath9k firmware (#727702)

* Sun Jun 05 2011 Peter Lemenkov <lemenkov@gmail.com> 20110601-2
- Remove duplicated licensing files from /lib/firmware

* Wed Jun 01 2011 Dave Airlie <airlied@redhat.com> 20110601-1
- Latest firmware release with AMD llano support.

* Thu Mar 10 2011 Dave Airlie <airlied@redhat.com> 20110304-1
- update to latest upstream for radeon ni/cayman, drop nouveau fw we don't use it anymore

* Tue Feb 08 2011 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 20110125-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_15_Mass_Rebuild

* Tue Jan 25 2011 David Woodhouse <dwmw2@infradead.org> 20110125-1
- Update to linux-firmware-20110125 (new bnx2 firmware)

* Fri Jan 07 2011 Dave Airlie <airlied@redhat.com> 20101221-1
- rebase to upstream release + add new radeon NI firmwares.

* Thu Aug 12 2010 Hicham HAOUARI <hicham.haouari@gmail.com> 20100806-4
- Really obsolete ueagle-atm4-firmware

* Thu Aug 12 2010 Hicham HAOUARI <hicham.haouari@gmail.com> 20100806-3
- Obsolete ueagle-atm4-firmware

* Fri Aug 06 2010 David Woodhouse <dwmw2@infradead.org> 20100806-2
- Remove duplicate radeon firmwares; they're upstream now

* Fri Aug 06 2010 David Woodhouse <dwmw2@infradead.org> 20100806-1
- Update to linux-firmware-20100806 (more legacy firmwares from kernel source)

* Fri Apr 09 2010 Dave Airlie <airlied@redhat.com> 20100106-4
- Add further radeon firmwares

* Wed Feb 10 2010 Dave Airlie <airlied@redhat.com> 20100106-3
- add radeon RLC firmware - submitted upstream to dwmw2 already.

* Tue Feb 09 2010 Ben Skeggs <bskeggs@redhat.com> 20090106-2
- Add firmware needed for nouveau to operate correctly (this is Fedora
  only - do not upstream yet - we just moved it here from Fedora kernel)

* Wed Jan 06 2010 David Woodhouse <David.Woodhouse@intel.com> 20090106-1
- Update

* Fri Aug 21 2009 David Woodhouse <David.Woodhouse@intel.com> 20090821-1
- Update, fix typos, remove some files which conflict with other packages.

* Thu Mar 19 2009 David Woodhouse <David.Woodhouse@intel.com> 20090319-1
- First standalone kernel-firmware package.
